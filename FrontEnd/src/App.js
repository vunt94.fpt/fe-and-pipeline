import './App.css';
import EmpListing from './LangsList';

function App() {
  return (
    <div className="App">
      <EmpListing />
    </div>
  );
}

export default App;
